

## 鉴权模块
* 鉴权依赖shiro、jwt、SpringBoot
* 鉴权模块提供基础获取用户的接口，注入SpringBean,需要在使用的时候实现该接口，并实现获取用户、角色、权限等接口功能
* 登陆成功后会将用户登陆数据放入Context中，LoginUser为Context中的用户数据，如有扩展字段只需继承LoginUser,并在接口查询时返回扩展后的对象即可

## 如需使用该模块，可直接在maven中添加依赖
```
<dependency>
    <groupId>com.uboot.uyibai</groupId>
    <artifactId>uboot-auth</artifactId>
</dependency>
```
> auth模块中依赖了core，core中包含springboot、mybatisplus、swagger等基础依赖

## 使用说明（示例代码详见uboot-test）
1. shiro-customize-properties.yml
    - 该文件中可定义项目需要被排除的url，不需要鉴权行为的接口
2. 系统LoginUser类为登陆的用户信息，可以自行继承扩展【5】中获取当前用户信息的方法中会获取继承的LoginUser对象信息
3. login 登陆接口，登陆成功后需要将jwt返回给前端，前端在后续请求服务器端接口的时候需要在header中带上该token
    ```
    JwtUtil.sign(loginUser.getUsername(), loginUser.getPassword())
    ```
4. 项目中需实现IAuthService接口，实现根据用户唯一标示获取当前用户信息的接口
    ```
    public interface IAuthService {
        // 设置用户拥有的角色集合，比如“admin,test”
        Set<String> getUserRolesSet(String username);
        // 设置用户拥有的权限集合，比如“sys:role:add,sys:user:add”
        Set<String> getUserPermissionsSet(String username);
        // 查询用户信息
        LoginUser getUserByName(String username);
    }
    ```
5. 项目中如需获取当前用户信息可直接调用
    ```
   ExamContextProvider.getContext()
   ```
