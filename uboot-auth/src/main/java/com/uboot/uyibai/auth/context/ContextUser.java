package com.uboot.uyibai.auth.context;

import com.uboot.uyibai.auth.vo.LoginUser;
import lombok.Data;

/**
 * @author: LiYang
 * @Email: lyflyyvip@163.com
 * @create: 2020-03-26 20:44
 * @Description:
 * 当前登陆用户信息
 **/
@Data
public class ContextUser {

    private String token;

    private String username;

    private LoginUser loginUser;

}
