package com.uboot.uyibai.auth.context;

import com.uboot.uyibai.auth.vo.LoginUser;

/**
 * @author : Hui.Wang [huzi.wh@gmail.com]
 * @version : 1.0
 * @created on  : 2018/5/13
 * 当前登陆用户context，可以从中获取当前登陆用户信息
 */
public class ExamContextProvider {

    private static final ThreadLocal<ContextUser> econtext = new ThreadLocal();

    public static ContextUser getContext() {
        ContextUser vo = econtext.get();
        if (vo == null) {
            ContextUser ContextUser = new ContextUser();
            econtext.set(ContextUser);
            return ContextUser;
        }
        return vo;
    }

    /**
     * 获取当前登录用户Id
     *
     * @return 返回登录用户id
     */
    public static LoginUser getCurrentUser() {
        return getContext().getLoginUser();
    }

    public static String getToken() {
        return getContext().getToken();
    }


    public static String getCurrentUserName() {
        return getContext().getUsername();
    }


    public static void setContext(String token, String username, LoginUser loginUser) {
        ContextUser vo = getContext();
        vo.setToken(token);
        vo.setUsername(username);
        vo.setLoginUser(loginUser);
        econtext.set(vo);
    }

    public static void setToken(String token) {
        ContextUser vo = getContext();
        vo.setToken(token);
        econtext.set(vo);
    }

    public static void setLoginUser(LoginUser loginUser) {
        ContextUser vo = getContext();
        vo.setLoginUser(loginUser);
        econtext.set(vo);
    }

    public static void setUserName(String userName) {
        ContextUser vo = getContext();
        vo.setUsername(userName);
        econtext.set(vo);
    }


}
