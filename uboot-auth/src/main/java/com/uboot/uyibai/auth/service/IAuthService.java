package com.uboot.uyibai.auth.service;


import com.uboot.uyibai.auth.vo.LoginUser;

import java.util.Set;

/**
 * @author: LiYang
 * @Email: lyflyyvip@163.com
 * @create: 2020-03-25 18:47
 * @Description:
 **/
public interface IAuthService {
    // 设置用户拥有的角色集合，比如“admin,test”
    Set<String> getUserRolesSet(String username);
    // 设置用户拥有的权限集合，比如“sys:role:add,sys:user:add”
    Set<String> getUserPermissionsSet(String username);
    // 查询用户信息
    LoginUser getUserByName(String username);
}
