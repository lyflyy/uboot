package com.uboot.uyibai.auth.config;

import com.uboot.uyibai.core.handler.YamlPropertySourceFactory;
import lombok.Data;
import org.apache.shiro.util.AntPathMatcher;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

/**
 * @author: LiYang
 * @Email: lyflyyvip@163.com
 * @create: 2020-02-02 16:58
 * @Description:
 * 用户自定义拦截内容
 **/
@Data
@Configuration
@PropertySource(value = {"classpath:config/shiro-customize-properties.yml"}, factory = YamlPropertySourceFactory.class)
@ConfigurationProperties("shiro-customize")
public class ShiroCustomizeProperties {

    private List<String> excludeUrls;

    public Boolean contains(String url){
        for(String pattern: excludeUrls){
            AntPathMatcher antPathMatcher = new AntPathMatcher();
            if(antPathMatcher.match(pattern, url)){
                return true;
            }
        }
        return false;

    }

}
