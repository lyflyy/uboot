package com.uboot.uyibai.auth.vo;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author: LiYang
 * @Email: lyflyyvip@163.com
 * @create: 2020-03-25 17:58
 * @Description:
 * 通用的shiro登陆内容
 * 登陆成功后会将用户信息存储至localstorage -> context中
 * 该类可被继承，并且重写根据username获取用户信息的方法，返回该class子类对象
 * 继而在登陆成功后继承的对象会直接放进context中，供调用当前登陆对象方法
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class LoginUser {

    /**
     * 登录人id
     */
    private String id;

    /**
     * 登录人账号
     */
    private String username;

    /**
     * 登录人密码
     */
    private String password;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 性别（1：男 2：女）
     */
    private Integer sex;

    /**
     * 电话
     */
    private String phone;

    /**
     * 状态(1：正常 2：冻结 ）
     */
    private Integer status;

    private String delFlag;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 租户Id
     */
    private String tenantId;

}
