package com.uboot.uyibai.uboot.demo.web;

import com.uboot.uyibai.auth.vo.LoginUser;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: LiYang
 * @Email: lyflyyvip@163.com
 * @create: 2020-03-26 19:59
 * @Description:
 **/
@Data
public class MyLoginUser extends LoginUser {

    private String color;

}
