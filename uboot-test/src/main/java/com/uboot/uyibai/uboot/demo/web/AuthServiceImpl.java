package com.uboot.uyibai.uboot.demo.web;

import com.uboot.uyibai.auth.service.IAuthService;
import com.uboot.uyibai.auth.vo.LoginUser;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Set;

/**
 * @author: LiYang
 * @Email: lyflyyvip@163.com
 * @create: 2020-03-26 15:06
 * @Description:
 **/
@Service
public class AuthServiceImpl implements IAuthService {


    /**
     * 模拟登陆查询数据库返回用户信息
     * @param username
     * @return
     */
    @Override
    public Set<String> getUserRolesSet(String username) {
        return (Set<String>) Arrays.asList("role1", "role2");
    }


    /**
     * 模拟登陆查询数据库返回用户信息
     * @param username
     * @return
     */
    @Override
    public Set<String> getUserPermissionsSet(String username) {
        return (Set<String>) Arrays.asList("Permission1", "Permission2");
    }


    /**
     * 模拟登陆查询数据库返回用户信息
     * @param username
     * @return
     */
    @Override
    public LoginUser getUserByName(String username) {
        MyLoginUser loginUser = new MyLoginUser();
        loginUser.setId("id");
        loginUser.setPassword("password");
        loginUser.setUsername(username);
        loginUser.setStatus(1);
        loginUser.setColor("red or green");
        return loginUser;
    }
}
