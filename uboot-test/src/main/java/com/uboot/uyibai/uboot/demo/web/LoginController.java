package com.uboot.uyibai.uboot.demo.web;

import cn.hutool.json.JSONObject;
import com.uboot.uyibai.auth.context.ExamContextProvider;
import com.uboot.uyibai.auth.service.IAuthService;
import com.uboot.uyibai.auth.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author: LiYang
 * @Email: lyflyyvip@163.com
 * @create: 2020-03-26 14:46
 * @Description:
 **/
@RestController
@RequestMapping("/sys/")
public class LoginController {

    @Autowired
    private IAuthService service;

    @PostMapping("login")
    public JSONObject login(@RequestBody MyLoginUser loginUser){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userInfo", service.getUserByName(loginUser.getUsername()));
        jsonObject.put("jwt", JwtUtil.sign(loginUser.getUsername(), loginUser.getPassword()));
        return jsonObject;
    }

    @GetMapping("self")
    public Object test(){
        return ExamContextProvider.getContext();
    }

    @GetMapping("test1")
    public String test1(){
        return "test1";
    }


}
